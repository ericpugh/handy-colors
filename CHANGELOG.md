# Changelog

All notable changes to `handy-colors` will be documented in this file

## 1.0.0 - 2019-12-13

- initial release
