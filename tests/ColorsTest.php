<?php

namespace Handy\Colors\Tests;

use PHPUnit\Framework\TestCase;
use Handy\Utils\Colors\Palette\Ncs;
use Handy\Utils\Colors\ColorPicker;
use Handy\Utils\Colors\Color;

/**
 * Class ColorsTest
 *
 * @package Handy\Tests
 */
class ColorsTest extends TestCase
{
    /**
     * @test
     */
    public function getPaletteCountTest()
    {
        $picker = new ColorPicker(Ncs::getColors());
        $total_num_colors = $picker->countColorPalette();

        $this->assertSame(6, $total_num_colors);
    }

    /**
     * @test
     */
    public function getColorNameTest()
    {
        $picker = new ColorPicker();
        // An example hex color.
        $example_hex = '#83F600';
        $example_color = new Color($picker::fromHexToInt($example_hex));
        // Finds the closest HEX color in the current palette.
        $closest = $picker->closestColor($example_color);
        // Finds the name "lawngreen".
        $name = $picker->getColorName($closest);

        $this->assertSame('lawngreen', $name);
    }


}
